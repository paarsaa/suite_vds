Feature: A brief yet descriptive text of what is desired

    Some textual description of the business value of this feature goes
    here. The text is free-form.

    The description can span multiple paragraphs.

    Scenario: A first scenario in which the feature can be exercised

        Given some precondition is met
          And another precondition
         When some action is performed
         Then the results should be as expected
          And some other testable outcome is achieved
