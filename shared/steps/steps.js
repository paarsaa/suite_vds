// A quick introduction to implementing scripts for BDD tests:
//
// This file contains snippets of script code to be executed as the .feature
// file is processed. See the section 'Behaviour Driven Testing' in the 'API
// Reference Manual' chapter of the Squish manual for a comprehensive reference.
//
// The functions Given/When/Then/Step can be used to associate a script snippet
// with a pattern which is matched against the steps being executed. Optional
// table/multi-line string arguments of the step are passed via a mandatory
// 'context' parameter:
//
//   When("I enter the text", function(context) {
//     <code here>
//   });
//
// The pattern is a plain string without the leading keyword, but a couple of
// placeholders including |any|, |word| and |integer| are supported which can
// be used to extract arbitrary, alphanumeric and integer values resp. from the
// pattern; the extracted values are passed as additional arguments:
//
//   Then("I get |integer| different names", function(context, numNames) {
//     <code here>
//   });
//
// Instead of using a string with placeholders, a regular expression object can
// be passed to Given/When/Then/Step to use regular expressions.
//

    Given("some precondition is met", function(context) {
        startApplication("com.volvo.vds");
    });

    Given("another precondition", function(context) {
        tapObject(waitForObject(":VDS.PLAIN_ToggleButton"), 89, 32);
        tapObject(waitForObject(":VDS.SPICY_ToggleButton"), 81, 30);
        tapObject(waitForObject(":VDS.EXTRA CHUNKY_ToggleButton"), 73, 3);
        tapObject(waitForObject(":VDS_Panel"), 101, 211);
        tapObject(waitForObject(":VDS.USER DEFINED_ToggleButton"), 125, 23);
        tapObject(waitForObject(":VDS.DEFAULT_ToggleButton"), 113, 18);
    });
    
    When("some action is performed", function(context) {
        tapObject(waitForObject(":VDS.PLAIN_ToggleButton"), 115, 46);
    });
    
    
    
    Then("some other testable outcome is achieved", function(context) {
        waitFor("object.exists(':VDS.SPICY_ToggleButton')", 20000);
        test.compare(findObject(":VDS.SPICY_ToggleButton").text, "SPICY");
    });

    Then("the results should be as expected", function(context) {
        waitFor("object.exists(':VDS.EXTRA CHUNKY_ToggleButton')", 20000);
        test.compare(findObject(":VDS.EXTRA CHUNKY_ToggleButton").text, "EXTRA CHUNKY");
        waitFor("object.exists(':VDS.USER DEFINED_ToggleButton')", 20000);
        test.compare(findObject(":VDS.USER DEFINED_ToggleButton").text, "USER DEFINED");
        waitFor("object.exists(':VDS.DEFAULT_ToggleButton')", 20000);
        test.compare(findObject(":VDS.DEFAULT_ToggleButton").text, "DEFAULT");
    });